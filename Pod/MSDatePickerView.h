//
//  MSDatePickerView.h
//  App
//
//  Created by miss on 2017/11/9.
//  Copyright © 2017年 miss. All rights reserved.
//

// DatePickerView

#import <UIKit/UIKit.h>

typedef void(^confirmButtonClickBlock)(NSString *date);

@interface MSDatePickerView : UIView

// 默认日期，默认 1990-09-01
@property (nonatomic, strong) NSDate *date;

// 最小日期
@property (nonatomic, strong) NSDate *minDate;

// 标题，默认空
@property (nonatomic, copy) NSString *title;

// 按钮颜色，默认（50，50，50）
@property (nonatomic, strong) UIColor *buttonColor;

// 标题颜色，默认（50，50，50）
@property (nonatomic, strong) UIColor *titleColor;

// 确认按钮回调，日期格式：yyyy-MM-dd
@property (nonatomic, copy) void(^confirmButtonBlock)(NSString *date);

// 显示 PickerView
- (void)showPickerView;

@end

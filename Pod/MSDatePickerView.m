//
//  MSDatePickerView.m
//  App
//
//  Created by miss on 2017/11/9.
//  Copyright © 2017年 miss. All rights reserved.
//

#import "MSDatePickerView.h"

#define MSPK_SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define MSPK_SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define MSPK_BGVIEW_WIDTH self.bgView.frame.size.width
#define MSPK_BGVIEW_HEIGHT self.bgView.frame.size.height
#define MSPK_IPHONEX MSPK_SCREEN_HEIGHT == 812

@interface MSDatePickerView ()

@property (nonatomic, copy) confirmButtonClickBlock confirmButtonClickBlock;
@property (nonatomic, weak) UIDatePicker *datePicker;
@property (nonatomic, weak) UIView *bgView;
@property (nonatomic, strong) NSArray<UIButton *> *buttonArray;
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation MSDatePickerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = CGRectMake(0, 0, MSPK_SCREEN_WIDTH, MSPK_SCREEN_HEIGHT);
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        
        CGFloat height = MSPK_IPHONEX?(frame.size.height+34):frame.size.height;
        UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, MSPK_SCREEN_HEIGHT, frame.size.width, height)];
        bgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:bgView];
        self.bgView = bgView;
        
        [self setUpSubView];
        
    }
    return self;
}

- (void)setUpSubView {
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, MSPK_BGVIEW_WIDTH, 45)];
    topView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    [self.bgView addSubview:topView];
    
    UIButton *cancelButton = [self buttonWithFrame:CGRectMake(0, 0, 60, topView.frame.size.height) title:@"取消"];
    [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:cancelButton];
    UIButton *confirmButton = [self buttonWithFrame:CGRectMake(MSPK_BGVIEW_WIDTH-60, 0, 60, topView.frame.size.height) title:@"确定"];
    [confirmButton addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:confirmButton];
    self.buttonArray = @[cancelButton, confirmButton];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 0, MSPK_BGVIEW_WIDTH-120, topView.frame.size.height)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor colorWithRed:50/255.0 green:50/255.0 blue:50/255.0 alpha:1];
    [topView addSubview:titleLabel];
    self.titleLabel = titleLabel;
    
    UIDatePicker *datePicker= [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 40, MSPK_BGVIEW_WIDTH, MSPK_BGVIEW_HEIGHT-(MSPK_IPHONEX?80:40))];
    NSString *date = @"1990-09-01";
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    datePicker.date = [formatter dateFromString:date];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [self.bgView addSubview:datePicker];
    self.datePicker = datePicker;
}

- (void)setDate:(NSDate *)date {
    self.datePicker.date = date;
}

- (void)setMinDate:(NSDate *)minDate {
    self.datePicker.minimumDate = minDate;
}

- (void)setButtonColor:(UIColor *)buttonColor {
    [self.buttonArray enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setTitleColor:buttonColor forState:UIControlStateNormal];
    }];
}

- (void)setTitleColor:(UIColor *)titleColor {
    self.titleLabel.textColor = titleColor;
}

- (void)showPickerView {
    [[UIApplication sharedApplication].delegate.window addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        CGRect rect = self.bgView.frame;
        rect.origin.y = MSPK_SCREEN_HEIGHT-MSPK_BGVIEW_HEIGHT;
        self.bgView.frame = rect;
    }];
}

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
}

- (void)removeMSPickerView {
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        CGRect rect = self.bgView.frame;
        rect.origin.y = MSPK_SCREEN_HEIGHT;
        self.bgView.frame = rect;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)cancel {
    [self removeMSPickerView];
}

- (void)confirm {
    [self removeMSPickerView];
    if (self.confirmButtonClickBlock) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [formatter stringFromDate:self.datePicker.date];
        self.confirmButtonClickBlock(dateString);
    }
}

- (void)confirmButtonClick:(confirmButtonClickBlock)block {
    self.confirmButtonClickBlock = block;
}

- (UIButton *)buttonWithFrame:(CGRect)frame title:(NSString *)title {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = frame;
    [button setTitleColor:[UIColor colorWithRed:50/255.0 green:50/255.0 blue:50/255.0 alpha:1] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    return button;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    CGPoint point = [touches.anyObject locationInView:self];
    if (point.y < (MSPK_SCREEN_HEIGHT - MSPK_BGVIEW_HEIGHT)) {
        [self removeMSPickerView];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
